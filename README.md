# classboard-backend

Backend for the Classboard app.

## Development

Start a MongoDB instance:

```
mongod;
```

Install packages:

```
npm install
```

Run the server:

```
npm start
```

## Documentation

You can check the documentation for the models [here](models.md).
