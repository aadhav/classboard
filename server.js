const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

// read from environmental variables
require('dotenv').config();

// setup express
const app = express();
const port = process.env.PORT || 5000;

// setup CORS and other middlewares
app.use(cors());
app.use(express.json());

// connect to MongoDB using Mongoose
const uri = process.env.MONGO_URI;
mongoose.connect(uri, { useNewUrlParser: true });
const connection = mongoose.connection;
connection.once('open', () => {
    console.log('connected to MongoDB successfully!');
});

/////////////////
// Setup routes
/////////////////

const usersRouter = require('./routes/users');
const classesRouter = require('./routes/classes');
const enrolledClassesRouter = require('./routes/enrolled_classes');
const classContentsRouter = require('./routes/class_contents');
const commentsRouter = require('./routes/comments');
const quizzesRouter = require('./routes/quizzes');
const quizResponsesRouter = require('./routes/quiz_responses');
const assignmentsRouter = require('./routes/assignments');
const assignmentResponsesRouter = require('./routes/assignment_responses');

app.use('/users', usersRouter);
app.use('/classes', classesRouter);
app.use('/enrolled_classes', enrolledClassesRouter);
app.use('/class_contents', classContentsRouter);
app.use('/comments', commentsRouter);
app.use('/quizzes', quizzesRouter);
app.use('/quiz_responses', quizResponsesRouter);
app.use('/assignments', assignmentsRouter);
app.use('/assignment_responses', assignmentResponsesRouter);

app.use(function(err, req, res, next) {
    console.log(err);
    res.status(500).send({"error": err.message});
});

app.listen(port, () => {
    console.log(`server is running on port ${port}`);
});
