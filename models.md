# Models

This doc describes the models used by the backend server:

`assignment_response`

| **Field** | **Type** | **Description** |
|---|---|---|
| assignmentFileUrl | string | The URL for the assignment's submission (PDF, etc.) |
| marks | string | Marks for the assignment |
| name | string | Name of the assignment |
| type | string | -- |
| userId | string | ID of the user who has submitted the assignment |
| assignment_id | string | ID of the assignment |

`assignment`

| **Field** | **Type** | **Description** |
|---|---|---|
| title | string | Assignment title |
| instruction | string | Instructions of the assignment |
| assignmentFileUrl | string | The URL for the assignment's submission (PDF, etc.) |
| assignmentMarks | string | Marks for the assignment |
| name | string | Name of the assignment |
| type | string | -- |
| startingDate | string | Start/creation date of the assignment |
| dueDate | string | Due date of the assignment |
| authorId | string | ID of the author who created the assignment |
| classId | string | ID of the class which has been assigned for the completion of the assignment |

`class_content`

| **Field** | **Type** | **Description** |
|---|---|---|
| rawText | string | Class content description |
| attachedFileUrls | array | List of attachments as file URLs |
| classId | string | ID for the class |
| userId | string | ID of the user |
| postedAt | string | Posted at (timestamp) |


`class`

| **Field** | **Type** | **Description** |
|---|---|---|
| className | string | Class name |
| section | string | Section |
| subject | string | Subject name |
| room | string | Room details |
| joinCode | string | Join Code |
| user | string | User type |


`comment`

| **Field** | **Type** | **Description** |
|---|---|---|
| comment | string | Comment |
| classId | string | Class ID |
| contentId | string | Content ID |
| userId | string | User ID |
| postedAt | string | Posted at (timestamp) |


`enrolled_class`

| **Field** | **Type** | **Description** |
|---|---|---|
| classCode | string | Class code |
| joinCode | string | Join code for the class |
| joinedAt | string | Joined at (timestamp) |
| userId | string | User ID |

`quiz_response`

| **Field** | **Type** | **Description** |
|---|---|---|
| userAnswer | array | User's answer |
| totalCorrect | number | Total number of correct answers |
| totalWrong | number | Total number of incorrect answers |
| userId | string | User ID |
| quizId | string | Quiz ID |


`quiz`

| **Field** | **Type** | **Description** |
|---|---|---|
| title | string | Quiz title |
| instruction | string | Quiz instructions |
| quiz_questions | array | List of questions for the quiz |
| startingDate | string | Starting date for the quiz |
| acceptingQuiz | string | Quiz acceptance status |
| authorId | string | Author ID |
| classId | string | Class ID |


`user`

| **Field** | **Type** | **Description** |
|---|---|---|
| email | string | User's email ID |
| fullName | string | User's full name |
| mobileNo | string | User's mobile number |
| isTeacher | boolean | Toggle to check if the user is a teacher or not |
| universityId | string | University ID |
| password | string | User's password |
